variable "aws_account_id" {}
variable "aws_access_key_id" {}
variable "aws_secret_access_key" {}
variable "aws_region" {}
variable "heroku_email" {}
variable "heroku_api_key" {}
variable "jwt_secret" {}
variable "orcid_base_path" {}
variable "orcid_client_secret" {}
variable "orcid_client_id" {}
variable "frontend_url" {}

terraform {
  backend "s3" {
    bucket = "terraform-remote-state-flockademic.com"
    key = "terraform.tfstate"
    encrypt = true
  }
}

provider "aws" {
  access_key = "${var.aws_access_key_id}"
  secret_key = "${var.aws_secret_access_key}"
  region = "${var.aws_region}"
}

provider "heroku" {
  email = "${var.heroku_email}"
  api_key = "${var.heroku_api_key}"
}

resource "heroku_app" "heroku_api_app" {
  name = "fl9-${terraform.workspace}"
  region = "eu"
  buildpacks = [
    "heroku/nodejs"
  ]

  config_vars {
    aws_account_id = "${var.aws_account_id}"
    aws_access_key_id = "${var.aws_access_key_id}"
    aws_secret_access_key = "${var.aws_secret_access_key}"
    aws_region = "${var.aws_region}"
    orcid_base_path = "${var.orcid_base_path}"
    orcid_client_id = "${var.orcid_client_id}"
    orcid_client_secret = "${var.orcid_client_secret}"
    jwt_secret = "${var.jwt_secret}"
    frontend_url = "${var.frontend_url}"
    article_upload_bucket = "${aws_s3_bucket.articles_bucket.id}"
    asset_upload_bucket = "${aws_s3_bucket.assets_bucket.id}"
  }
}

resource "heroku_addon" "database" {
  app  = "${heroku_app.heroku_api_app.name}"
  plan = "heroku-postgresql:hobby-dev"
}

output "heroku_web_url" {
  value = "${heroku_app.heroku_api_app.web_url}"
}
output "heroku_git_url" {
  value = "${heroku_app.heroku_api_app.git_url}"
}
output "heroku_hostname" {
  value = "${heroku_app.heroku_api_app.heroku_hostname}"
}

resource "aws_s3_bucket" "articles_bucket" {
  bucket = "${terraform.workspace}-uploaded-articles"
  acl = "public-read"
  # Allows deleting the bucket even when there are files in it:
  force_destroy = true
  policy = <<POLICY
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Sid": "AllowPublicRead",
      "Action": [
        "s3:GetObject"
      ],
      "Effect": "Allow",
      "Resource": "arn:aws:s3:::${terraform.workspace}-uploaded-articles/*",
      "Principal": "*"
    }
  ]
}
POLICY

  cors_rule {
    # allowed_headers should not be necessary, but I'm leaving it in in case it doesn't work so I can try it
    # allowed_headers = ["*"]
    allowed_methods = ["GET", "PUT"]
    allowed_origins = ["${var.frontend_url}", "https://flockademic.com", "https://www.flockademic.com"]
    max_age_seconds = 3000
  }

  versioning {
    enabled = true
  }
}

resource "aws_s3_bucket" "assets_bucket" {
  bucket = "${terraform.workspace}-uploaded-assets"
  acl = "public-read"
  # Allows deleting the bucket even when there are files in it:
  force_destroy = true
  policy = <<POLICY
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Sid": "AllowPublicRead",
      "Action": [
        "s3:GetObject"
      ],
      "Effect": "Allow",
      "Resource": "arn:aws:s3:::${terraform.workspace}-uploaded-assets/*",
      "Principal": "*"
    }
  ]
}
POLICY

  cors_rule {
    # allowed_headers should not be necessary, but I'm leaving it in in case it doesn't work so I can try it
    # allowed_headers = ["*"]
    allowed_methods = ["GET", "PUT"]
    allowed_origins = ["${var.frontend_url}", "https://flockademic.com", "https://www.flockademic.com"]
    max_age_seconds = 3000
  }

  versioning {
    enabled = false
  }
}
