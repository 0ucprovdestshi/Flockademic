import * as React from 'react';

import { StaticRouter } from 'react-router';
import { BrowserRouter } from 'react-router-dom';

export const PrerenderableRouter = (props: React.Props<any>) => (
  // Since our unit test setup emulates a browser environment,
  // we can't test the server-side version properly.
  // Unfortunately, the next statement doesn't appear to work currently,
  // so we're using the Jest option `coveragePathIgnorePatterns`
  // as a workaround.
  /* istanbul ignore else */
  (typeof document !== 'undefined')
    ? <BrowserRouter>{props.children}</BrowserRouter>
    : <StaticRouter>{props.children}</StaticRouter>
);
